;;; early-init.el --- GNU Emacs Early Initialization File -*- lexical-binding: t; -*-

;; Copyright (c) 2023 Andrew Scott

;; Author: Andrew Scott <andy at andyscott dot me>
;; Keywords: convenience, tools
;; URL: https://codeberg.org/andyscott/dotfiles

;; This file is not part of GNU Emacs.

;;; Commentary:

;; My customizations for early Emacs initialization

;;; License:

;; MIT No Attribution

;; Permission is hereby granted, free of charge, to any person obtaining a copy of this
;; software and associated documentation files (the "Software"), to deal in the Software
;; without restriction, including without limitation the rights to use, copy, modify,
;; merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
;; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
;; PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Code:

;; Disable `package.el'
(setq package-enable-at-startup nil)

;; Ensure other defaults are loaded
(setq inhibit-default-init nil)

;; Avoid `garbage-collect' during init
(setq gc-cons-percentage 1
      gc-cons-threshold most-positive-fixnum)

;; Skip regexp checking by `find-file-name-handler' during init
(defvar default-file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)

;; Restore `garbage-collect' and `file-name-handler-alist' settings
(add-hook 'elpaca-after-init-hook
          (lambda ()
            (setq file-name-handler-alist default-file-name-handler-alist
                  gc-cons-percentage 0.1
                  gc-cons-threshold (* 1024 1024 8))))

;; Relocate `emacs-user-directory' to XDG_DATA_HOME
(setq user-emacs-directory
      (expand-file-name "emacs/" (or (getenv "XDG_DATA_HOME") "~/.local/share/")))

;; Set cache directory
(setq xdg_cache_home
      (expand-file-name "emacs/" (or (getenv "XDG_CACHE_HOME") "~/.cache/")))
(unless (file-directory-p xdg_cache_home)
  (make-directory xdg_cache_home))

;; Move eln-cache to XDG_CACHE_HOME
(when (fboundp 'startup-redirect-eln-cache)
  (if (< emacs-major-version 29)
      (push (expand-file-name "eln-cache/" xdg_cache_home) native-comp-eln-load-path)
    (startup-redirect-eln-cache (expand-file-name "eln-cache/" xdg_cache_home))))

;; Load newest byte code
(setq load-prefer-newer t)

;; Enable async native compilation and suppress warnings
(when (featurep 'native-compile)
  (setq native-comp-jit-compilation t
        native-comp-async-report-warnings-errors nil))

;; Don't advertise instructions for frame exit
(setq server-client-instructions nil)

;; Don't implicitly resize the frame
(setq frame-inhibit-implied-resize t)

;; Allow frames to increase/decrease by one pixel
(setq frame-resize-pixelwise t)

;; Disable startup screen
(setq inhibit-startup-screen t)

;; Start in `fundamental-mode' to prevent loading of non-essential packages
(setq initial-major-mode 'fundamental-mode)

;; Visible bell in mode-line only
(setq ring-bell-function
      (lambda ()
        (let ((orig_fg (face-foreground 'mode-line)))
          (set-face-foreground 'mode-line "#F2804F")
          (run-with-idle-timer 0.1 nil
                               (lambda (fg) (set-face-foreground 'mode-line fg))
                               orig_fg))))

;; Fonts
;; (set-face-font 'default "Hack-14")
;; (set-face-font 'fixed-pitch "Hack-14")
;; (set-face-font 'variable-pitch "DejaVu Sans-14")

;; Some UI options
(let ((options '((font . "Hack-14")
                 (undecorated . t)
                 (menu-bar-lines . 0)
                 (tool-bar-lines . 0)
                 (vertical-scroll-bars)
                 (background-color . "#282828")
                 (foreground-color . "#c6c6c6"))))
  (dolist (option options)
    (push option default-frame-alist)))

;; Make lsp-mode use plists
(setenv "LSP_USE_PLISTS" "true")

(provide 'early-init)
;;; early-init.el ends here
