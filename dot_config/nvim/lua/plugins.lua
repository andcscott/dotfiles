return {
  -- statusline
  {
    "nvim-lualine/lualine.nvim",
    config = true,
    dependencies = { "nvim-tree/nvim-web-devicons" }
  },
  -- Theme
  {
    "catppuccin/nvim",
    name = "catppuccin",
    priority = 1000,
    config = function()

      local macchiato = require("catppuccin.palettes").get_palette

    end,

  },
  {
    "neanias/everforest-nvim",
    version = false,
    priority = 1000,
    config = function()
      require("everforest").setup({
              background = "soft",
              transparent_background_level = 0,
              italics = false,
              disable_italic_comments = false,
      })
    end,
  },
  -- Which-key keybind hints 
  { 
    "folke/which-key.nvim",
    config = true,
  },
  -- Auto-pair brackets, parenthesis, etm.
  {
    "windwp/nvim-autopairs",
    config = true,
    lazy = true,
    event = { "BufReadPost", "BufNewFile" }
  },
  -- Tree-sitter
  {
    "nvim-treesitter/nvim-treesitter",
    lazy = true,
    build = ":TSUpdate",
    event = { "BufReadPost", "BufNewFile" },
    opts = {
      indent = { enable = true },
      highlight = { enable = true },
      auto_install = { enable = true },
      ensure_installed = {
        "bash",
        "c",
        "cpp",
        "lua",
        "luadoc",
        "markdown",
        "python",
        "vim",
        "vimdoc",
        "yaml",
        "zig",
      },
    },
  },
  -- Auto-completion
  {
    "hrsh7th/nvim-cmp",
    event = "InsertEnter",
    dependencies = {
      {"L3MON4D3/LuaSnip"},
    },
    config = function()
      -- Here is where you configure the autocompletion settings.
      -- The arguments for .extend() have the same shape as `manage_nvim_cmp`: 
      -- https://github.com/VonHeikemen/lsp-zero.nvim/blob/v2.x/doc/md/api-reference.md#manage_nvim_cmp

      require("lsp-zero.cmp").extend()

      -- And you can configure cmp even more, if you want to.
      local cmp = require("cmp")
      local cmp_action = require("lsp-zero.cmp").action() 

      cmp.setup({
        mapping = {
          ["<C-Space>"] = cmp.mapping.complete(),
          ["<C-f>"] = cmp_action.luasnip_jump_forward(),
          ["<C-b>"] = cmp_action.luasnip_jump_backward(),
        }
      })
    end
  },
  -- LSP
  {
    "VonHeikemen/lsp-zero.nvim",
    branch = "v2.x",
    lazy = true,
    config = function()
      -- This is where you modify the settings for lsp-zero
      -- Note: autocompletion settings will not take effect

      require("lsp-zero.settings").preset({})
    end
  },
  {
    "neovim/nvim-lspconfig",
    cmd = "LspInfo",
    event = {"BufReadPre", "BufNewFile"},
    dependencies = {
      {"hrsh7th/cmp-nvim-lsp"},
      {"williamboman/mason-lspconfig.nvim"},
      {
        "williamboman/mason.nvim",
        lazy = true,
        build = function()
          pcall(vim.api.nvim_command, "MasonUpdate")
        end,
      },
    },
    config = function()
      -- This is where all the LSP shenanigans will live

      local lsp = require("lsp-zero")

      lsp.on_attach(function(client, bufnr)
        -- see :help lsp-zero-keybindings
        -- to learn the available actions
        lsp.default_keymaps({buffer = bufnr})
      end)
      
      lsp.setup_servers({'pyright', 'zls'})

      -- (Optional) Configure lua language server for neovim
      -- require("lspconfig").lua_ls.setup(lsp.nvim_lua_ls())

      lsp.setup()
    end
  }
}

